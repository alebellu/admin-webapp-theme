; this project has been created with ...
; to build dev version: lein cljsbuild auto dev
; to build release version: lein cljsbuild auto release
; to build the css: lein garden auto
; to clean all: lein clean
(ns ^:figwheel-always admin-webapp-theme.core
  (:require [cljs.core.async :as async :refer [<! chan close! put!]]
            [cljs.core.async.impl.protocols :refer [Channel]]
            [cljs-time.core :as t]
            [cljs-time.coerce :as c]
            [cljs-time.format :as f]
            [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)]
            [reagent.core :as reagent :refer [atom]]
            [ssoup-clj-common.core :as ssoup]
            [ssoup-clj-client.core :as sc]
            [ssoup-clj-client.session :as session]
            [ssoup-clj-client.auth :as auth]
            [ssoup-clj-client.i18n :refer [msg]]
            [ssoup-webapp.core :as sw]
            [admin-webapp-theme.scs-rules]
            [admin-webapp-theme.module])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defn logout-succeeded []
  (sw/logout))

(defn logout-failed [])

(defn page-header [_ options]
  [:div
   [:div#main-menu
    (sc/container :admin-webapp/main-menu)]])

(defn main-menu [ctx options]
  (let [on-change (:on-change options)
        enum-type (:obj-type options)
        enum-values (:enum-values enum-type)
        app-full-name (:app-full-name ctx)                  ; (:full-name @sw/app-info)
        app-short-name (:app-short-name ctx)]               ; (:short-name @sw/app-info)
    [:nav#perfplus-navbar.navbar.navbar-inverse.navbar-static-top
     [:div.container-fluid
      [:div.navbar-header
       [:button.navbar-toggle.collapsed {:type "button" :data-toggle "collapse" :data-target "#perfplus-navbar-collapse" :class "collapsed"}
        [:span.sr-only "Toggle navigation"]
        [:span.icon-bar]
        [:span.icon-bar]
        [:span.icon-bar]]
       [:span.navbar-brand.hidden-xs app-full-name]
       [:span.navbar-brand.visible-xs app-short-name]
       [:div.header-logo]]
      ; Collect the nav links, forms, and other content for toggling
      [:div#perfplus-navbar-collapse.navbar-collapse.collapse.in
       [:ul.nav.navbar-nav
        ;[:li [:a.contact {:href "http://support.3esynaptiq.com/" :title "contact" :target "_blank"} "Contact"]]
        ;[:li [:a.contact {:onClick #(auth/logout! logout-succeeded logout-failed) :title "contact" :target "_blank"} "Logout"]]
        (for [enum-value enum-values]
          ^{:key (:key enum-value)} [:li [:a {:onClick #(on-change (:key enum-value)) :title "contact" :target "_blank"} (:label enum-value)]])
        ]]]]))

(defn page-footer [_ options]
  [:div.footer {:id "footer"}
   [:hr]
   [:p [:img {:src (:logo-image @sw/app-info) :style {:margin-right "20px"}}]
    (:full-name @sw/app-info) " © " (:year-from @sw/app-info) "-" (t/year (t/now)) " "
    [:a {:href (:company-website @sw/app-info) :target "_blank" :style {:color "#A3A2A2"}} (:company-name @sw/app-info)]]])

(defn empty-viewer [ctx data options]
  [:div.nil])

(defn datatable-render [ctx data options]
   [:table#example.table.table-striped.table-bordered {:cell-spacing "0" :width "100%"}
    [:thead
     [:tr
      (for [key (:keys data)]
        ^{:key key} [:th (name key)])]]
    [:tbody
     (for [item (:items data)]
       ^{:key (:uuid item)} [:tr
        (for [key (:keys data)]
          ^{:key key} [:td [sc/show ctx (get item (keyword key))]])])]])

(defn datatable-did-mount [ctx data options]
  (.ready (js/$ js/document) (fn []
                               (.DataTable (js/$ "#example")))))

(defn datatable-viewer [ctx data options]
  (info "call dtb " data)
  (reagent/create-class {:render              (fn [] (datatable-render ctx data options))
                         :component-did-mount (fn [] (datatable-did-mount ctx data options))}))

(defn dashboard-2-2-1 [ctx data options]
  "A vector viewer displaying items in a 2-2-1 dashboard layout"
  (let [size (ssoup/array-count data)]
    [:div.col-xs-12
     [:div.col-xs-12
      [:div.col-sm-6 (when (> size 0) [sc/show ctx (ssoup/array-nth data 0)])]
      [:div.col-sm-6 (when (> size 1) [sc/show ctx (ssoup/array-nth data 1)])]]
     [:div.col-xs-12
      [:div.col-sm-6 (when (> size 2) [sc/show ctx (ssoup/array-nth data 2)])]
      [:div.col-sm-6 (when (> size 3) [sc/show ctx (ssoup/array-nth data 3)])]]
     [:div.col-xs-12
      [:div.col-sm-12 (when (> size 4) [sc/show ctx (ssoup/array-nth data 4)])]]]))

(defn dashboard-1-2 [ctx data options]
  "A vector viewer displaying items in a 1-2 dashboard layout"
  (let [size (ssoup/array-count data)]
    [:div.col-xs-12
     [:div.col-xs-12
      [:div.col-sm-12 (when (> size 0) [sc/show ctx (ssoup/array-nth data 0)])]]
     [:div.col-xs-12
      [:div.col-sm-6 (when (> size 1) [sc/show ctx (ssoup/array-nth data 1)])]
      [:div.col-sm-6 (when (> size 2) [sc/show ctx (ssoup/array-nth data 2)])]]]))

(defn dashboard-viewer [ctx data options]
  "A vector viewer displaying items in a dashboard layout"
  (let [layout (:layout options)]
    (cond (= "2-2-1" layout) (dashboard-2-2-1 ctx data options)
          (= "1-2" layout)   (dashboard-1-2 ctx data options)
          :else              (dashboard-2-2-1 ctx data options))))

(defn flow-viewer [ctx data options]
  "A vector viewer displaying items in a flow layout"
  [:div.col-xs-12
   (for [item data]
     ^{:key (:uuid item)} [sc/show ctx item])])

(defn main-template [ctx options]
  [:div#main.container-fluid
   [:div#main-div.col-xs-12
    [page-header ctx options]
    ; [sc/show ctx (:root landing-object)]
    (sc/container :admin-webapp/main-content)
    [:div.col-xs-12
     [page-footer ctx options]]]])

(defn- string-viewer [_ str options]
  [:span str])

(defn- string-selector [_ options]
  [:div
   [:input {:type      "text"
            :on-change #(let [on-change (:on-change options)]
                          (when on-change
                            (on-change (-> % .-target .-value))))}]])

(defn- array-selector [ctx options]
  (let [select-items (:select-items options)
        on-change (:on-change options)
        selected-items (atom #{})]
    [:div.col-xs-12
     (for [select-item select-items]
         ^{:key (:uuid select-item)} [:div
                                      [:input {:type     "checkbox"
                                               :on-click #(when on-change
                                                           (if (-> % .-target .-checked)
                                                             (swap! selected-items conj select-item)
                                                             (swap! selected-items disj select-item))
                                                           (on-change @selected-items))}]
                                      [sc/show ctx #{:ssoup/compact} select-item]])]))

(defn init-theme [ctx options]
  (debug "Registering theme admin-webapp")

  (sc/register-theme ctx :ssoup/admin-webapp-theme
                     :layout-manager (sc/->DefaultLayoutManager)
                     :viewers [[:admin-webapp/empty-viewer nil nil empty-viewer]
                               [:admin-webapp/string-viewer nil :ssoup/string string-viewer]
                               [:admin-webapp/flow-viewer nil :ssoup/array flow-viewer]
                               [:admin-webapp/dashboard-viewer nil :ssoup/array dashboard-viewer]
                               [:admin-webapp/datatable-viewer #{:admin-webapp/data-table} :ssoup/array datatable-viewer]]
                     :editors []
                     :selectors [[:admin-webapp/string-selector nil :ssoup/string string-selector]
                                 [:admin-webapp/array-selector nil :ssoup/array array-selector]
                                 [:admin-webapp/main-menu #{:admin-webapp/main-menu} :ssoup/enum main-menu]]
                     :templates [[:admin-webapp/main-template main-template]]))

(derive :ssoup/admin-webapp-theme :ssoup/default-theme)

(ssoup/register {:fname     :admin-webapp-theme/init-theme
                 :handler   (fn [ctx args options]
                              (init-theme ctx options))
                 :flavor    :ssoup/admin-webapp-theme
                 :user-role :ssoup/any-user
                 :classes   #{:ssoup/init-theme}
                 :arg-types {}})
