(ns ^:figwheel-always admin-webapp-theme.styles.all
  (:require [garden.def :refer [defstylesheet defstyles]]
            [garden.units :refer [px]]
            [garden.stylesheet :refer [at-media]]))

(def synaptiq-green "#19b432")
(def synaptiq-dark-green "#09a422")
(def error-red "#aa0000")

(defstyles all
[:hr {
  :border "1px solid #ddd"
  :margin "8px"}]

[:.error {:color error-red :text-decoration "underline"}]

[:.green {:color synaptiq-green}]

[:a {:color synaptiq-green :text-decoration "underline"}]

[:a:hover {:color synaptiq-green :cursor "pointer"}]

[:div#main.container-fluid {:padding "0px"}]

[:div#main-div {:padding "0px"}]

[:.btn-primary {
  :background-image "none"
  :background-color synaptiq-green
  :border-color synaptiq-dark-green
  :text-decoration "none"}
 [:&:focus :&:hover {
    :background-color synaptiq-dark-green
    :border-color synaptiq-dark-green}]
 [:&:disabled {
    :background-color synaptiq-dark-green
    :border-color synaptiq-dark-green}]]

[:.vcenter {
  :display "inline-block"
  :vertical-align "middle"
  :float "none"}]

[:.font-size-10 {:font-size "10px"}]
[:.font-size-11 {:font-size "11px"}]
[:.font-size-12 {:font-size "12px"}]
[:.font-size-13 {:font-size "13px"}]
[:.font-size-14 {:font-size "14px"}]
[:.font-size-15 {:font-size "15px"}]
[:.font-size-16 {:font-size "16px"}]
[:.font-size-17 {:font-size "17px"}]
[:.font-size-18 {:font-size "18px"}]
[:.font-size-19 {:font-size "19px"}]
[:.font-size-20 {:font-size "20px"}]

[:.pull-down :.pull-down-10 {:margin-top "10px"}]
[:.pull-down-5 {:margin-top "5px"}]
[:.pull-down-20 {:margin-top "20px"}]
[:.pull-down-30 {:margin-top "30px"}]
[:.pull-down-40 {:margin-top "40px"}]
[:.pull-down-50 {:margin-top "50px"}]

[:.header {
  :height "39px"
  :position "relative"
  :background "#3c3c3c url(images/header_arrow-down_20x7.png) no-repeat 110px top"
  :border-top (str "4px solid " synaptiq-green)}]

[:.header-title {
  :position "absolute"
  :top "3px"
  :left "40px"
  :height "35px"
  :color "#eee"
  :font-size "19px"
  :font-family "Helvetica Neue', Helvetica, Arial, sans-serif"}]

[:.header-logo {
  :position "absolute"
  :margin-top "-5px"
  :right "100px"
  :width "185px"
  :height "60px"
  :background "url(images/header_logo_185x60.png) no-repeat 0 0"}]

[:#perfplus-navbar {
  :height "39px"
  :background "#333 url(images/header_arrow-down_20x7.png) no-repeat 110px top"
  :border-top (str "4px solid " synaptiq-green)}
 [:a {
   :text-shadow "none"
   :text-decoration "underline"}]
 [:a:hover {
   :color synaptiq-green}]
 [:.navbar-brand {
  :color "#eee"
  :font-size "19px"
  :font-family "Helvetica Neue, Helvetica, Arial, sans-serif"}]
 [:.header-logo {
  :position "absolute"
  :margin-top "-4px"
  :right "100px"
  :width "185px"
  :height "60px"
  :background "url(images/header_logo_185x60.png) no-repeat 0 0"}]
 [(at-media {:max-width "768px"}
  [:.navbar-collapse {
    :background-color "#333"}])]
 [:.navbar-toggle {
  :background-color "#333"}
  [:.icon-bar {
   :background-color "#888"}]]
 [:.navbar-toggle:hover {
  :background-color "#555"
   }]
 [:.contact {
  :background "url(images/header_contact_19x13.png) no-repeat center left"
  :color "#A3A2A2"
  :padding "15px 0px 10px 29px"
  :margin-left "50px"
  :font-family "'Droid Serif', arial, serif"
  :line-height "17px"}]]

[:.footer {
  :color "#A3A2A2"
  :height "65px"
  :outline-color "#A3A2A2"
  :outline-style "none"
  :outline-width "0px"
  :font-family "Arial"
  :font-size "11px"
  :font-stretch "normal"
  :font-style "normal"
  :font-variant "normal"
  :font-weight "normal"
  :letter-spacing "1px"
  :line-height "12px"
  :text-align "center"}]

[:#switchPanel
 [:h3 {
  :margin-top "12px"}]]

[:#controlPanel {
  ; :border-top "2px solid lightGrey"
}]

[:#chartPanel {
  ; :border-top "2px solid lightGrey"
  :background-color "lightGray"
  :height "auto"
  :padding-bottom "10px"}
 [:#chart {
    :height "450px"
    :padding "0"
    :background-color "#bbb"}]]

[:.icon-excel {
  :background "url(images/excel.gif) no-repeat center left"
}]

[:.indicatorviewer
 {:height "25px"}]

[:.indicatoreditor
 {:height "30px"}
 [:a {
    :text-decoration "none"}]])
