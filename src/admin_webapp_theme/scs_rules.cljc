(ns ^:figwheel-always admin-webapp-theme.scs-rules
  (:require [taoensso.timbre :as timbre :refer-macros (log trace debug info warn error fatal)]))

; :show[:ssoup-webapp/landing-object] > :show[:ssoup/array] {
;   :fname :ssoup-webapp/dashboard-viewer }

(defn rules []
  [{:selector [{:classes [:ssoup/show], :arg-types [:ssoup-webapp/landing-object]}
               {:modifier ">", :classes [:ssoup/show], :arg-types [:ssoup/array]}]
      :rule     {:fname :ssoup-webapp/dashboard-viewer}}
   {:selector [{:fname :ssoup-webapp/dashboard-viewer}
               {:classes [:ssoup/show], :arg-types [:ssoup/array]}]
    :rule     {:fname :ssoup-webapp/datatable-viewer}}])

(def simpler-rules
  [#{:landing-object}
   [:dashboard-viewer #{:ssoup/array} {"option1" :xyz, "option2" :xyz2}
     [:...
      [:datatable-viewer #{:ssoup/array}]]]])
